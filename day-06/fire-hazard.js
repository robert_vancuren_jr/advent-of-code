
function FireHazard() {
    this.canvas = document.createElement("canvas");
    this.canvas.width = 1000;
    this.canvas.height = 1000;
    this.context = this.canvas.getContext("2d");

    this.context.fillStyle = "red";

}

FireHazard.prototype.rectangleFromPoints = function(startX, startY, endX, endY) {
    var rectangle = {
        x: parseInt(startX),
        y: parseInt(startY),
        width: endX - startX + 1,
        height: endY - startY + 1
    };

    return rectangle;
};

FireHazard.prototype.turnOn = function(startX, startY, endX, endY) {
    var rect = this.rectangleFromPoints(startX, startY, endX, endY);
    
    this.context.globalCompositeOperation = "source-over";
    this.fillRectangle(rect);
};

FireHazard.prototype.turnOff = function(startX, startY, endX, endY) {
    var rect = this.rectangleFromPoints(startX, startY, endX, endY);

    this.context.globalCompositeOperation = "destination-out";
    this.fillRectangle(rect);
};

FireHazard.prototype.toggle = function(startX, startY, endX, endY) {
    var rect = this.rectangleFromPoints(startX, startY, endX, endY);

    this.context.globalCompositeOperation = "xor";
    this.fillRectangle(rect);
};

FireHazard.prototype.fillRectangle = function(rect) {
    this.context.fillRect(rect.x, rect.y, rect.width, rect.height);
};

FireHazard.prototype.numberOfPixelsOn = function() {
    var onCount = 0

    var imageData = this.context.getImageData(
        0, 0, this.canvas.width, this.canvas.height);

    var pixelInfo = imageData.data;
    var length = pixelInfo.length;
    for (var i = 0; i < length; i+=4) {
        var redComponent = pixelInfo[i];

        if (redComponent == 255) {
            onCount++;
        }
    }

    return onCount;
};
