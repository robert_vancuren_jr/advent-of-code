var md5 = require("js-md5");
var secretKey = "yzbqklnj";

function startsWithFiveZeros(hash) {
    return hash.indexOf("000000") == 0;
}

function mineAdventCoin() {
    var currentNumber = 1;
    var stringToHash = secretKey + currentNumber;

    var hash = md5(stringToHash);

    while(startsWithFiveZeros(hash) == false) {
        currentNumber++;
        stringToHash = secretKey + currentNumber;

        hash = md5(stringToHash);
    }
    
    return currentNumber;
}

console.info(mineAdventCoin());
