var input = "3113322113";

console.time("part1");
var play1Sequence = play(input, 40);
console.timeEnd("part1");
console.info(play1Sequence.length);

console.time("part2");
var play2Sequence = play(input, 50);
console.timeEnd("part2");
console.info(play2Sequence.length);

function play(startSequence, turns) {
    var afterReading = startSequence;
    for (var i = 0; i < turns; i++) {
        afterReading = readAloud(afterReading);
    }

    return afterReading;
}

function takeTurn(sequence) {
    var afterReading = readAloud(sequence);
    return afterReading;
}

function readAloud(sequence) {
    var newSequence = "";
    for (var i = 0; i < sequence.length; i++) {
        var characterToCount = sequence[i];
        var count = countCharacterAt(i, sequence);
        newSequence += count;
        newSequence += characterToCount;

        i += count - 1;
    }
    return newSequence;
}

function countCharacterAt(index, sequence) {
    var characterToCount = sequence[index];
    var count = 1;
    var nextIndex = index + 1;
    while (sequence[nextIndex] === characterToCount) {
        nextIndex++;
        count++;
    }

    return count;
}
