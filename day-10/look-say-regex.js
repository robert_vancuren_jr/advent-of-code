var input = "3113322113";

var regex = /(\d)\1*/g;

console.time("part1");
var play1Sequence = play(input, 40);
console.timeEnd("part1");
console.info(play1Sequence.length);

console.time("part2");
var play2Sequence = play(input, 50);
console.timeEnd("part2");
console.info(play2Sequence.length);

function play(startSequence, turns) {
    var afterReading = startSequence;
    for (var i = 0; i < turns; i++) {
        afterReading = readAloud(afterReading);
    }

    return afterReading;
}

function readAloud(sequence) {
    var match = sequence.match(regex);
    return match.map(say).join("");
}

function say(string) {
    return string.length + string[0];
}
