var fs = require("fs");

console.info("Matchsticks");

fs.readFile("input.txt", readFileHandler);

function readFileHandler(error, data) {
    var input = splitFileIntoLines(data);
    console.info("input length", input.length);

    doIt(input);
}

function splitFileIntoLines(fileData) {
    var lines = fileData.toString().split("\r\n");
    lines.pop(); //extra line at the end

    return lines;
}

function doIt(input) {
    decode(input);
    encode(input);
}

function decode(input) {
    var codeTotal = 0;
    var memoryTotal = 0;
    for (var i = 0; i < input.length; i++) {
        var codeLength = input[i].length;
        var inMemoryLength = decodeString(input[i]);
        codeTotal += codeLength;
        memoryTotal += inMemoryLength;
    }

    var answer = codeTotal - memoryTotal;
    console.info(codeTotal);
    console.info(memoryTotal);
    console.info("total", answer);
}

function encode(input) {
    var beforeTotal = 0;
    var afterTotal = 0;
    for (var i = 0; i < input.length; i++) {
        var before = input[i].length;
        var after = encodeString(input[i]);
        beforeTotal += before;
        afterTotal += after;
    }

    var answer = afterTotal - beforeTotal;
    console.info(afterTotal);
    console.info(beforeTotal);
    console.info("total", answer);
}

function encodeString(string) {
    var replaced = string;

    replaced = JSON.stringify(string)

    return replaced.length;
}

function decodeString(string) {
    var replaced = string;

    replaced = replaced.replace(/\\\\/g, "a");
    replaced = replaced.replace(/^"|"$/g, "");
    replaced = replaced.replace(/\\x../g, "1")
    replaced = replaced.replace(/\\\"/g, 'a');

    return replaced.length;
}
