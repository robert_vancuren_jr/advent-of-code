//TODO better handle wires vs number as input

/* output
wire a -> 956
---Part 2-------
wire a -> 40149
*/

var fs = require("fs");
var maxSignal = 1 << 16;

function Wire(name) {
    this.name = name
}

Wire.prototype.getValue = function() {
    if (this.value === undefined) {
        this.value = this.keepInRange(this.evaluate());
    }

    return this.value;
};

Wire.prototype.keepInRange = function(value) {
    return ((value % maxSignal) + maxSignal) % maxSignal;
};


var input = [
    "123 -> x",
    "456 -> y",
    "x AND y -> d",
    "x OR y -> e",
    "x LSHIFT 2 -> f",
    "y RSHIFT 2 -> g",
    "NOT x -> h",
    "NOT y -> i",
    "x -> lol",
];

function bitwiseAnd(a, b) {
    return a & b;
}

function bitwiseOr(a, b) {
    return a | b;
}

function bitwiseLeft(a, b) {
    return a << b;
}

function bitwiseRight(a, b) {
    return a >> b;
}

function bitwiseNot(a) {
    return ~a;
}

var opToFunc = {
    "AND": bitwiseAnd,
    "OR": bitwiseOr,
    "LSHIFT": bitwiseLeft,
    "RSHIFT": bitwiseRight,
};

var wires = {};
//build(input);
//outputWires();

fs.readFile("input.txt", readFileHandler);

function readFileHandler(error, data) {
    var input = splitFileIntoLines(data);

    build(input);

    var wireA = getWire("a");
    console.info("----------");
    var valueOfA = wireA.getValue();
    console.info("wire a ->", valueOfA);

    wires = {};
    build(input);
    var wireB = getWire("b");
    wireB.evaluate = parseInt.bind(this, valueOfA);

    console.info("---Part 2-------");
    var wireA2 = getWire("a");
    console.info("wire a ->", wireA2.getValue());

}

function splitFileIntoLines(fileData) {
    var lines = fileData.toString().split("\r\n");
    lines.pop(); //extra line at the end
    console.info(lines.length);

    return lines;
}

function outputWires() {
    console.info("-------");
    for (var key in wires) {
        console.info(key, "->", wires[key].getValue());
    }
}

function getWire(name) {
    var wire = wires[name];
    if (typeof wire === "undefined") {
        var wire = new Wire(name);
        wires[name] = wire;
    }

    return wire;
}

function build(input) {
    for (var i = 0; i < input.length; i++) {
        var instruction = input[i];
        buildWire(instruction);
    }
}

function buildWire(instruction) {
    if (instruction.search(/AND|OR/) >= 0) {
        buildAndOr(instruction);
    } else if (instruction.search(/LSHIFT|RSHIFT/) >= 0) {
        buildLeftRight(instruction);
    } else if (instruction.search(/NOT/) >= 0) {
        buildNot(instruction);
    } else {
        setWire(instruction);
    }
}

function setWire(instruction) {
    var parts = instruction.split(" ");

    var input1 = parts[0];
    var wire = getWire(parts[2]);

    if (isNaN(input1)) {
        input1 = getWire(input1);

        wire.evaluate = function() {
            return input1.getValue();
        };
    } else {
        input1 = parseInt(input1);
        wire.evaluate = parseInt.bind(this, input1);
    }
}

function buildAndOr(instruction) {
    var parts = instruction.split(" ");
    var input1 = parts[0];

    var input2 = getWire(parts[2]);
    var operation = parts[1];

    var output = getWire(parts[4]);

    if (isNaN(input1)) {
        input1 = getWire(input1);

        output.evaluate = function() {
            return opToFunc[operation](input1.getValue(), input2.getValue());
        }
    } else {
        input1 = parseInt(input1);
        output.evaluate = function() {
            return opToFunc[operation](input1, input2.getValue());
        }
    }
}

function buildLeftRight(instruction) {
    var parts = instruction.split(" ");
    var input1 = getWire(parts[0]);
    var input2 = parseInt(parts[2]);
    var operation = parts[1];

    var output = getWire(parts[4]);
    output.evaluate = function() {
        return opToFunc[operation](input1.getValue(), input2);
    }
}

function buildNot(instruction) {
    var parts = instruction.split(" ");
    var input1 = getWire(parts[1]);

    var output = getWire(parts[3]);
    output.evaluate = function() {
        return bitwiseNot(input1.getValue());
    }
}
