function loadFile(url, callback) {
    window.loadFileCallback = callback;

    var request = new XMLHttpRequest();
    request.addEventListener("load", loadHandler);
    request.open("GET", url);
    request.responseType = "text";
    request.send();
}

function loadHandler(event) {
    var request = event.currentTarget;
    window.loadFileCallback(request.response);
}
