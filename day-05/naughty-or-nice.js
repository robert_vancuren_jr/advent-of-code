var fs = require("fs");

console.info("naughty or nice");

var threeVowels = /([aeiou])\w*([aeiou])\w*([aeiou])/;
var twoInARow = /(.)\1/;
var naughtyWords = /(ab)|(cd)|(pq)|(xy)/;

var pairNoOverlap = /(..).*\1/;
var pairWithOneBetween = /(.).{1}\1/;

fs.readFile("input.txt", readFileHandler);

function readFileHandler(error, data) {
    var wordList = splitFileIntoLines(data);

    part1(wordList);
    console.info("----------------");
    part2(wordList);
}

function splitFileIntoLines(fileData) {
    var lines = fileData.toString().split("\r\n");
    lines.pop(); //extra line at the end

    return lines;
}

function part1(wordList) {
    console.info("part 1");
    var numberOfNiceWords = howManyAreNice(wordList);
    console.info(numberOfNiceWords);
}

function part2(wordList) {
    console.info("part 2");
    var numberOfNiceWords2 = howManyAreNicePart2(wordList);
    console.info(numberOfNiceWords2);
}

function howManyAreNice(words) {
    return words.filter(isNice).length;
}

function isNice(string) {
    return containsAtLeastThreeVowels(string)
    && containsADoubleLetter(string)
    && doesNotContainNaughtyWords(string);
}

function howManyAreNicePart2(words) {
    return words.filter(isNicePart2).length;
}

function isNicePart2(string) {
    return containsPairNoOverLap(string)
    && containsRepeatLetterWithOneBetween(string);
}

function containsAtLeastThreeVowels(string) {
    return containsMatch(string, threeVowels);
}

function containsADoubleLetter(string) {
    return containsMatch(string, twoInARow);
}

function doesNotContainNaughtyWords(string) {
    return !(containsMatch(string, naughtyWords));
}

function containsPairNoOverLap(string) {
    return containsMatch(string, pairNoOverlap);
}

function containsRepeatLetterWithOneBetween(string) {
    return containsMatch(string, pairWithOneBetween);
}

function containsMatch(string, regex) {
    return string.match(regex) != null;
}
