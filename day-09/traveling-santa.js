var fs = require("fs");

console.info("traveling santa");

fs.readFile("input.txt", readFileHandler);

var distanceMap = {};
var cityList = [];

function readFileHandler(error, data) {
    var input = splitFileIntoLines(data);

    findSantaARoute(input);
}

function splitFileIntoLines(fileData) {
    var lines = fileData.toString().split("\r\n");
    lines.pop(); //extra line at the end

    return lines;
}

function findSantaARoute(input) {
    input.forEach(parseData);
    cityList = cityList.filter(removeDuplicates);

    var allPaths = getPermutations(cityList); 

    var shortest = findShortest(allPaths);
    console.info("shortest", shortest);

    var longest = findLongest(allPaths);
    console.info("longest", longest);
}

function findShortest(allPaths) {
    var distances = allPaths.map(pathToDistance);
    var shortest = distances.reduce(min);
    return shortest;
}

function findLongest(allPaths) {
    var distances = allPaths.map(pathToDistance);
    var longest = distances.reduce(max);
    return longest;
}

function min(previous, current) {
    return Math.min(previous, current);
}

function max(previous, current) {
    return Math.max(previous, current);
}

function pathToDistance(path) {
    var total = 0;
    for (var i = 0; i < path.length - 1; i++) {
        total += getDistance(path[i], path[i+1]);
    }

    return total;
}

function getDistance(cityOne, cityTwo) {
    return distanceMap[cityOne][cityTwo];
}

function parseData(locationPair) {
    var parts = locationPair.split(" ");
    var cityOne = parts[0];
    var cityTwo = parts[2];

    var distanceBetween = parseInt(parts[4]);

    addToMap(cityOne, cityTwo, distanceBetween);
}

function addToMap(cityOne, cityTwo, distanceBetween) {
    //map both ways
    if (distanceMap[cityOne] === undefined) {
        distanceMap[cityOne] = {};
    }
    if (distanceMap[cityTwo] === undefined) {
        distanceMap[cityTwo] = {};
    }

    distanceMap[cityOne][cityTwo] = distanceBetween;
    distanceMap[cityTwo][cityOne] = distanceBetween;

    cityList.push(cityOne);
    cityList.push(cityTwo);
}

function getPermutations(array) {
    var result = [];
    if (array.length === 1) {
        return [array];
    }

    for (var i = 0; i < array.length; i++) {
        var subArray = array.slice(0, i).concat(array.slice(i + 1));
        var subPermutations = getPermutations(subArray);

        for (var j = 0; j < subPermutations.length; j++) {
            subPermutations[j].unshift(array[i]);
            result.push(subPermutations[j]);
        }
        
    }

    return result;
}

function removeDuplicates(element, index, array) {
    return array.indexOf(element) == index;
}
